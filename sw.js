const cacheName = "formula-1"
const filesToCache = ['index.html','html/driver.html','js/index.js','js/driver.js', 'css/index.css','css/driverpages.css','images/driver_photos/albon.jpg',
'images/driver_photos/bottas.jpg','images/driver_photos/gasly.jpg','images/driver_photos/giovinazzi.jpg','images/driver_photos/grosjean.jpg','images/driver_photos/hamilton.jpg',
'images/driver_photos/kvyat.jpg','images/driver_photos/latifi.jpg','images/driver_photos/leclerc.jpg','images/driver_photos/magnussen.jpg','images/driver_photos/norris.jpg',
'images/driver_photos/ocon.jpg','images/driver_photos/perez.jpg','images/driver_photos/raikkonen.jpg','images/driver_photos/ricciardo.jpg','images/driver_photos/russell.jpg',
'images/driver_photos/sainz.jpg','images/driver_photos/stroll.jpg','images/driver_photos/verstappen.jpg','images/driver_photos/vettel.jpg','images/team_logo/alfaromeo.jpg',
'images/team_logo/alphatauri.jpg','images/team_logo/ferrari.jpg','images/team_logo/haas.jpg','images/team_logo/mclaren.jpg','images/team_logo/mercedes.jpg',
'images/team_logo/racingpoint.jpg','images/team_logo/redbull.jpg','images/team_logo/renault.jpg','images/team_logo/williams.jpg','audio/bottas_radio_check.mp3',
'audio/hamilton_7_times.mp3','audio/lando_sings.mp3','audio/sainz_p1.mp3','audio/super_max.mp3','audio/vettel_sings.mp3','video/albon_crash.mp4','video/bottas_grass.mp4',
'video/carlos_vs_gasly.mp4','video/hamilton_3_wheels.mp4','video/kimi_pit.mp4','video/lando_wins.mp4','video/leclerc_loses_spot.mp4','video/pierre_gasly_wins.mp4','video/russell.mp4',
'video/stroll_poll.mp4','video/vettel_dog.mp4','/']

/* Start the service worker and cache app's resources */
self.addEventListener('install', (e) => {
    e.waitUntil(
        caches.open(cacheName).then((cache) => {
            return cache.addAll(filesToCache);
        })
    );
});

/* Serve cached resources when offline */
self.addEventListener('fetch', (e) => {
    e.respondWith(
        caches.match(e.request).then((response) => {
            return response || fetch(e.request);
        })
    );
});