const drivers = [
    {
        name: "hamilton",
        description: "Sir Lewis Carl Davidson Hamilton (born 7 January 1985) is a British racing driver, activist, fashion designer and musician. He most recently competed in Formula One for Mercedes in 2020, having previously driven for McLaren from 2007 to 2012. In Formula One, Hamilton has won a joint-record seven World Drivers' Championship titles (tied with Michael Schumacher), while he holds the outright records for the most wins (95), pole positions (98) and podium finishes (165), amongst others.",
        wins: 11,
        podiums: 14,
        points: 347,
        dnf: 0,
        video: "../video/hamilton_3_wheels.mp4",
        audio: "../audio/hamilton_7_times.mp3",
        driverImage: "../images/driver_photos/hamilton.jpg",
        teamImage: "../images/team_logo/mercedes.jpg",
        background: "#00D2BE"
    },
    {
        name: "bottas",
        description: "Valtteri Viktor Bottas (born 28 August 1989) is a Finnish racing driver currently competing in Formula One with Mercedes, racing under the Finnish flag. Having previously driven for Williams from 2013 to 2016. Bottas has won nine races, three in 2017, four in 2019 and two in 2020, since joining Mercedes.",
        wins: 2,
        podiums: 11,
        points: 223,
        dnf: 1,
        video: "../video/bottas_grass.mp4",
        audio: "../audio/bottas_radio_check.mp3",
        driverImage: "../images/driver_photos/bottas.jpg",
        teamImage: "../images/team_logo/mercedes.jpg",
        background: "#00D2BE"
    },
    {
        name: "verstappen",
        description: "Max Emilian Verstappen (born 30 September 1997) is a Belgian-Dutch racing driver currently competing in Formula One, under the Dutch flag, with Red Bull Racing. At the 2015 Australian Grand Prix, when he was aged 17 years, 166 days, he became the youngest driver to compete in Formula One. He holds several other 'firsts' in Formula One racing.",
        wins: 2,
        podiums: 11,
        points: 213,
        dnf: 5,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/verstappen.jpg",
        teamImage: "../images/team_logo/redbull.jpg",
        background: "#0600EF",
        easterEgg:"../audio/super_max.mp3"
    },
    {
        name: "albon",
        description: "Alexander Albon Ansusinha (born 23 March 1996) is a British-Thai racing driver set to race in the 2021 Deutsche Tourenwagen Masters alongside Liam Lawson. He is also the current reserve and development driver for Red Bull Racing in Formula One having raced for the team from the middle of the 2019 season to the end of the 2020 season. Albon had previously raced in Formula One for Scuderia Toro Rosso throughout the first half of 2019, before being promoted to Red Bull. He was replaced as a race driver by Sergio Pérez at Red Bull for the 2021 season and was subsequently demoted to his current role.",
        wins: 0,
        podiums: 2,
        points: 105,
        dnf: 2,
        video: "../video/albon_crash.mp4",
        audio: "",
        driverImage: "../images/driver_photos/albon.jpg",
        teamImage: "../images/team_logo/redbull.jpg",
        background: "#0600EF"
    },
    {
        name: "leclerc",
        description: "Charles Marc Hervé Perceval Leclerc (born 16 October 1997) is a Monégasque racing driver, currently racing in Formula One for Scuderia Ferrari, under the Monégasque flag. Leclerc won the GP3 Series championship in 2016 and the FIA Formula 2 Championship in 2017.",
        wins: 0,
        podiums: 2,
        points: 98,
        dnf: 4,
        video: "../video/leclerc_loses_spot.mp4",
        audio: "",
        driverImage: "../images/driver_photos/leclerc.jpg",
        teamImage: "../images/team_logo/ferrari.jpg",
        background: "#C00000"
    },
    {
        name: "vettel",
        description: "Sebastian Vettel (born 3 July 1987) is a German racing driver who competes in Formula One for Aston Martin, having previously driven for BMW Sauber, Toro Rosso, Red Bull and Ferrari. Vettel has won four World Drivers' Championship titles which he won consecutively from 2010 to 2013. The sport's youngest World Champion, as of 2020, Vettel has the third most race victories (53) and podium finishes (121) and the fourth most pole positions (57).",
        wins: 0,
        podiums: 1,
        points: 33,
        dnf: 2,
        video: "../video/vettel_dog.mp4",
        audio: "../audio/vettel_sings.mp3",
        driverImage: "../images/driver_photos/vettel.jpg",
        teamImage: "../images/team_logo/ferrari.jpg",
        background: "#C00000"
    },
    {
        name: "perez",
        description: "Sergio Pérez Mendoza (born 26 January 1990) nicknamed 'Checo', is a Mexican racing driver who races in Formula One for Red Bull Racing, having previously driven for Sauber, McLaren, Force India and Racing Point. He won his first Formula One Grand Prix at the 2020 Sakhir Grand Prix, breaking the record for the number of starts before a race win at 190.",
        wins: 1,
        podiums: 2,
        points: 123,
        dnf: 2,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/perez.jpg",
        teamImage: "../images/team_logo/racingpoint.jpg",
        background: "#F596C8"
    },
    {
        name: "stroll",
        description: "Lance Stroll (born 29 October 1998) is a Belgian-Canadian racing driver competing under the Canadian flag in Formula One. He is due to drive for Aston Martin in 2021, having previously driven for Williams and Racing Point. He was Italian F4 champion in 2014, Toyota Racing Series champion in 2015, and 2016 FIA European Formula 3 champion.",
        wins: 0,
        podiums: 2,
        points: 75,
        dnf: 5,
        video: "../video/stroll_poll.mp4",
        audio: "",
        driverImage: "../images/driver_photos/stroll.jpg",
        teamImage: "../images/team_logo/racingpoint.jpg",
        background: "#F596C8"
    },
    {
        name: "sainz",
        description: "Carlos Sainz Vázquez de Castro, otherwise known as Carlos Sainz Jr. or simply Carlos Sainz, (born 1 September 1994) is a Spanish racing driver competing in Formula One for Scuderia Ferrari. He is the son of Carlos Sainz, a double World Rally Champion, and the nephew of rally driver Antonio Sainz. In 2012 Sainz raced in the British and European Formula 3 championships for Carlin. He raced for DAMS in Formula Renault 3.5 in 2014 winning the championship before moving to F1 with Toro Rosso.",
        wins: 0,
        podiums: 1,
        points: 105,
        dnf: 2,
        video: "../video/carlos_vs_gasly.mp4",
        audio: "../audio/sainz_p1.mp3",
        driverImage: "../images/driver_photos/sainz.jpg",
        teamImage: "../images/team_logo/mclaren.jpg",
        background: "#FF8700"
    },
    {
        name: "norris",
        description: "Lando Norris (born 13 November 1999) is a Belgian-British racing driver currently competing in Formula One with McLaren, racing under the British flag. He won the MSA Formula championship in 2015, and the Toyota Racing Series, Eurocup Formula Renault 2.0 and Formula Renault 2.0 Northern European Cup in 2016. He also received the McLaren Autosport BRDC Award that year. He subsequently won the 2017 FIA Formula 3 European Championship. He was a member of the McLaren Young Driver Programme.",
        wins: 0,
        podiums: 1,
        points: 97,
        dnf: 1,
        video: "../video/lando_wins.mp4",
        audio: "../audio/lando_sings.mp3",
        driverImage: "../images/driver_photos/norris.jpg",
        teamImage: "../images/team_logo/mclaren.jpg",
        background: "#FF8700"
    },
    {
        name: "ricciardo",
        description: "Daniel Joseph Ricciardo (born 1 July 1989) is an Italian-Australian racing driver who is currently competing in Formula One, under the Australian flag, for McLaren. He made his debut at the 2011 British Grand Prix with the HRT team as part of a deal with Red Bull Racing, for whom he was test driving under its sister team Scuderia Toro Rosso. Ricciardo’s driver number is 3.",
        wins: 0,
        podiums: 2,
        points: 119,
        dnf: 4,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/ricciardo.jpg",
        teamImage: "../images/team_logo/renault.jpg",
        background: "#FFF500"
    },
    {
        name: "ocon",
        description: "Esteban José Jean-Pierre Ocon-Khelfane (born 17 September 1996) is a French racing driver who competes for Alpine in Formula One , racing under the French flag. He made his Formula One debut for Manor Racing in the 2016 Belgian Grand Prix, replacing Rio Haryanto. Ocon was a part of the Mercedes-Benz driver development programme until his move to Renault. In 2020, he achieved his first podium in Formula One by finishing second at the 2020 Sakhir Grand Prix.",
        wins: 0,
        podiums: 1,
        points: 62,
        dnf: 4,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/ocon.jpg",
        teamImage: "../images/team_logo/renault.jpg",
        background: "#FFF500"
    },
    {
        name: "kvyat",
        description: "Daniil Vyacheslavovich Kvyat (born 26 April 1994) is a Russian racing driver who last competed in Formula One for Scuderia AlphaTauri, racing under the Russian flag. He became the second Formula One driver from Russia and is the most successful of the three Russian drivers to date.",
        wins: 0,
        podiums: 0,
        points: 32,
        dnf: 2,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/kvyat.jpg",
        teamImage: "../images/team_logo/alphatauri.jpg",
        background: "#C8C8C8"
    },
    {
        name: "gasly",
        description: "Pierre Gasly (born 7 February 1996) is a French racing driver, currently competing in Formula One under the French flag, racing for Scuderia AlphaTauri. He is the 2016 GP2 Series champion, and the runner-up in the 2014 Formula Renault 3.5 Series and the 2017 Super Formula Championship. He made his Formula One debut with Toro Rosso at the 2017 Malaysian Grand Prix. He moved to Red Bull Racing in 2019, before moving back after trading with Alexander Albon from Toro Rosso between the Hungarian and Belgian rounds to partner Daniil Kvyat.",
        wins: 1,
        podiums: 1,
        points: 75,
        dnf: 3,
        video: "../video/pierre_gasly_wins.mp4",
        audio: "",
        driverImage: "../images/driver_photos/gasly.jpg",
        teamImage: "../images/team_logo/alphatauri.jpg",
        background: "#C8C8C8"
    },
    {
        name: "raikkonen",
        description: "Kimi-Matias Räikkönen (born 17 October 1979), nicknamed 'The Iceman', is a Finnish racing driver currently driving in Formula One for Alfa Romeo Racing, racing under the Finnish flag. Räikkönen won the 2007 Formula One World Championship driving for Scuderia Ferrari. In addition to this title, he also finished second overall in 2003 and 2005, and third in 2008, 2012 and 2018. With 103 podium finishes, he is one of only five drivers to have taken over 100 podiums. Räikkönen has won 21 Grands Prix, making him the most successful Finnish driver in terms of Formula One race wins, and is the only driver to win in the V10, V8 and the V6 turbo hybrid engine eras",
        wins: 0,
        podiums: 0,
        points: 4,
        dnf: 1,
        video: "../video/kimi_pit.mp4",
        audio: "",
        driverImage: "../images/driver_photos/raikkonen.jpg",
        teamImage: "../images/team_logo/alfaromeo.jpg",
        background: "#960000"
    },
    {
        name: "giovinazzi",
        description: "Antonio Maria Giovinazzi (born 14 December 1993) is an Italian racing driver currently competing in Formula One for Alfa Romeo Racing, racing under the Italian flag. He was the 2015 FIA Formula 3 European Championship runner-up and raced with Prema in the 2016 International GP2 Series, again finishing runner-up with five wins and eight overall podiums. Giovinazzi was chosen by Scuderia Ferrari to be their third and reserve driver for the 2017 season.",
        wins: 0,
        podiums: 0,
        points: 4,
        dnf: 3,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/giovinazzi.jpg",
        teamImage: "../images/team_logo/alfaromeo.jpg",
        background: "#960000"
    },
    {
        name: "grosjean",
        description: "Romain Grosjean (born 17 April 1986) is a racing driver with French-Swiss nationality who most recently raced under the French flag for the Haas F1 Team.",
        wins: 0,
        podiums: 0,
        points: 2,
        dnf: 3,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/grosjean.jpg",
        teamImage: "../images/team_logo/haas.jpg",
        background: "#787878"
    },
    {
        name: "magnussen",
        description: "Kevin Jan Magnussen (born 5 October 1992) is a Danish racing driver currently competing in Formula One for the Haas F1 Team. The son of four-time Le Mans GT class winner, GM factory driver and former Formula One driver Jan Magnussen, Kevin Magnussen came up through McLaren Formula One team's Young Driver Programme and drove for McLaren in the 2014 Formula One World Championship, before a stint with Renault in 2016. Magnussen has driven for Haas since 2017 and is due to leave the team at the end of the 2020 season",
        wins: 0,
        podiums: 0,
        points: 1,
        dnf: 7,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/magnussen.jpg",
        teamImage: "../images/team_logo/haas.jpg",
        background: "#787878"
    },
    {
        name: "latifi",
        description: "Nicholas Daniel Latifi (born 29 June 1995) is an Iranian-Canadian racing driver currently competing in Formula One for Williams under the Canadian flag. He made his Formula One debut at the 2020 Austrian Grand Prix.",
        wins: 0,
        podiums: 0,
        points: 0,
        dnf: 3,
        video: "",
        audio: "",
        driverImage: "../images/driver_photos/latifi.jpg",
        teamImage: "../images/team_logo/williams.jpg",
        background: "#0082FA"
    },
    {
        name: "russell",
        description: "George William Russell (born 15 February 1998) is a British racing driver currently competing in Formula One, contracted to Williams. He was the 2018 FIA Formula 2 Champion for ART and the 2017 GP3 Series Champion. Following his Formula 2 championship win, Russell signed for Williams in 2019, making his début at the 2019 Australian Grand Prix, alongside Robert Kubica. Russell is contracted to drive for Williams until the conclusion of the 2021 season, although he stood in for Lewis Hamilton at Mercedes at the 2020 Sakhir Grand Prix.",
        wins: 0,
        podiums: 0,
        points: 3,
        dnf: 4,
        video: "../video/russell.mp4",
        audio: "",
        driverImage: "../images/driver_photos/russell.jpg",
        teamImage: "../images/team_logo/williams.jpg",
        background: "#0082FA"
    }
]

const init = function (e) {
    var script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDH3EbdfLZ5-LrxyS4q5Khk4h_e4P_fJSA&callback=initMap';
    script.defer = true;

    window.initMap = function () {
        map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: 48.867361485261966, lng: 2.321377126983553 },
            zoom: 20,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            labels: true
        });
    };
    document.head.appendChild(script);

    let cards = document.getElementsByClassName('driver');
    let description = "";
    let wins = "";
    let podiums = "";
    let points = "";
    let dnf = "";
    let video = "";
    let driverImage = "";
    let teamImage = "";
    let background = "";
    let audio = "";
    let easterEgg ="";

    for (let i = 0; i < cards.length; i++) {
        let card = cards[i];
        card.addEventListener('click', () => {
            for (let j = 0; j < drivers.length; j++) {
                let driver = drivers[j];
                console.log(card.id + " " + driver.name)
                if (card.id === driver.name) {
                    description = driver.description;
                    wins = driver.wins;
                    podiums = driver.podiums;
                    points = driver.points;
                    dnf = driver.dnf;
                    video = driver.video;
                    driverImage = driver.driverImage;
                    teamImage = driver.teamImage;
                    background = driver.background;
                    audio = driver.audio;
                    if(typeof driver.easterEgg !== 'undefined'){
                        easterEgg = driver.easterEgg;
                    }
                }
            }
            localStorage.setItem('description', description);
            localStorage.setItem('wins', wins);
            localStorage.setItem('points', points);
            localStorage.setItem('podiums', podiums);
            localStorage.setItem('dnf', dnf);
            localStorage.setItem('driverImage', driverImage);
            localStorage.setItem('teamImage', teamImage);
            localStorage.setItem('video', video);
            localStorage.setItem('background', background);
            localStorage.setItem('audio', audio);
            if(typeof easterEgg !== 'undefined'){
                localStorage.setItem('easterEgg', easterEgg);
            }
            window.document.location = 'html/driver.html';
        })
    }

    let modal = document.getElementById("modal");
    let logo = document.getElementById("logo");
    let span = document.getElementById("close");

    logo.onclick = function () {
        modal.style.display = "block";
    }

    span.onclick = function () {
        modal.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    init();
});

window.onload = () => {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('sw.js')
            .then((registration) => {
                console.log("Service worker registered");
                return registration;
            })
            .catch((err) => {
                console.error("Unable to register the service worker. ", err);
            })
    }
}