const init = function (e) {
    const description = localStorage.getItem('description');
    const wins = localStorage.getItem('wins');
    const points = localStorage.getItem('points');
    const podiums = localStorage.getItem('podiums');
    const dnf = localStorage.getItem('dnf');
    const video = localStorage.getItem('video');
    const driverImage = localStorage.getItem('driverImage');
    const teamImage = localStorage.getItem('teamImage');
    const background = localStorage.getItem('background');
    const audio = localStorage.getItem('audio');
    const easterEgg = localStorage.getItem('easterEgg');

    let descNode = document.createTextNode(description);
    let winsNode = document.createTextNode("Won races: " + wins);
    let pointsNode = document.createTextNode("Points: " + points);
    let podiumsNode = document.createTextNode("Podiums: " + podiums);
    let dnfNode = document.createTextNode("DNF: " + dnf);

    let descEl = document.getElementById('desc');
    let statsUl = document.getElementById('stats');
    let liWins = document.createElement('li');
    let liPod = document.createElement('li');
    let liPoints = document.createElement('li');
    let liDnf = document.createElement('li');
    let videoDiv = document.getElementById('video');
    let audioDiv = document.getElementById('audio');

    if (video != '') {
        let videoEl = document.createElement('video');
        let source = document.createElement('source');
        let desc = document.createElement('h1');
        let text = document.createTextNode("Memorable video from the season: ");
        source.src = video;
        videoEl.autoplay = false;
        videoEl.id = "videoSrc";
        videoEl.controls = "controls";
        desc.appendChild(text);
        videoEl.appendChild(source);
        videoDiv.appendChild(desc);
        videoDiv.appendChild(videoEl);
    }
    if (audio != '') {
        let audioEl = document.createElement('audio');
        let source = document.createElement('source');
        let desc = document.createElement('h1');
        let text = document.createTextNode("Memorable audio from the season: ");
        source.src = audio;
        audioEl.autoplay = false;
        audioEl.id = "audioSrc";
        audioEl.controls = "controls";
        desc.appendChild(text);
        audioEl.appendChild(source);
        audioDiv.appendChild(desc);
        audioDiv.appendChild(audioEl);
    }

    liWins.appendChild(winsNode);
    liPod.appendChild(podiumsNode);
    liPoints.appendChild(pointsNode);
    liDnf.appendChild(dnfNode);

    descEl.appendChild(descNode);
    statsUl.appendChild(liWins);
    statsUl.appendChild(liWins);
    statsUl.appendChild(liPod);
    statsUl.appendChild(liPoints);
    statsUl.appendChild(liDnf);

    document.getElementById('driver').src = driverImage;
    document.getElementById('logo').src = teamImage;
    document.getElementById('body').style.backgroundColor = background;

    if (typeof easterEgg !== 'undefined') {
        let audioEl = document.createElement('audio');
        let source = document.createElement('source');
        source.src = easterEgg;
        audioEl.appendChild(source);
        audioEl.id = "easterEgg"
        document.getElementById('body').appendChild(audioEl);
        window.addEventListener('keydown', (e) => {
            if (e.code === 'KeyM') {
                document.getElementById('easterEgg').play();
            }
            if (e.code === 'KeyP') {
                document.getElementById('easterEgg').pause();
            }
        })
    }

    // let canvas = document.getElementById('canvas');
    // let ctx = canvas.getContext('2d');
    // canvas.width = window.innerWidth;
    // canvas.height = canvas.witdth;
    // let image = new Image();
    // image.src = driverImage;

    // image.onload = function () {
    //     ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
    // }
};

document.addEventListener('DOMContentLoaded', function () {
    init();
})